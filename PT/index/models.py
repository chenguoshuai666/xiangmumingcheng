from django.db import models


# Create your models here.

class Users(models.Model):
    username = models.CharField(max_length=10, unique=True, null=False)
    password = models.CharField(max_length=10, null=False)
    sex = models.IntegerField(default=2)